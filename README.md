Plataforma para que los padres o tutores puedan recibir notificaciones de los profesores sobre sus hij@s.

## Mapa del codigo ##

    plataform_colegio/
	    platform_colegio/ # app con algunas configuraciones globales
	    colegios/         # app de registro de colegios y cursos
		notificaciones/   # app para las notificaciones
		usuarios/         # app de gestion de usuarios del proyecto

Una vez clonado **quitar del seguimiento de git** el archivo `configuraciones.py` por que contiene configuraciones propias de cada servidor, para quitar del seguimiento ejecutar:

    git update-index --assume-unchanged platform_colegio/platform_colegio/configuraciones.py

Asi git no hara seguimiento a ese archivo incluso si cambia.

## setup rapido ##

1. clonar repositorio

2. instalar dependencias

        # en el directorio del proyecto
		virtualenv --python=python3 venv
		pip install django psycopg2 gunicorn

3. crear base de datos

        sudo -u postgres psql
		# iniciara la consola de postgres y alli:
		
        CREATE DATABASE plataform_colegio OWNER postgres;

        # por seguridad cambiar el password por defecto del usuario postgres
		\password postgres
		\q # para salir de la consola postgres

El nombre de la base de datos `plataform_colegio` debe estar en el archivo `/plataform_colegio/platform_colegio/configuraciones.py` y tambien el password.

4. crear las tablas

En bash:

        cd ruta/del/proyecto
		. venv/bin/activate
        cd plataform_colegio
		python manage.py makemigrations
		python manage.py migrate

Si todo sale bien:

        python manage.py runsever

Luego revisar que se hayan creado las tablas correctamente en postgres


