from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.core.mail import send_mail
from django.core.urlresolvers import reverse

from usuarios.models import UserM
from usuarios.models import Student, Principal, Teacher, Tutor

PERIODOS_ACADEMICOS=(
    ("SIN_DEFINIR","no definido"),
    # por trimestres
    ("2017-T1","2017 trimestre 1"), ("2017-T2","2017 trimestre 2"), ("2017-T3","2017 trimestre 3"),
    ("2018-T1","2018 trimestre 1"), ("2018-T2","2018 trimestre 2"), ("2018-T3","2018 trimestre 3"),
    ("2019-T1","2019 trimestre 1"), ("2019-T2","2019 trimestre 2"), ("2019-T3","2019 trimestre 3"),
    ("2020-T1","2020 trimestre 1"), ("2020-T2","2020 trimestre 2"), ("2020-T3","2020 trimestre 3"),
    ("2021-T1","2021 trimestre 1"), ("2021-T2","2021 trimestre 2"), ("2021-T3","2021 trimestre 3"),
    ("2022-T1","2022 trimestre 1"), ("2022-T2","2022 trimestre 2"), ("2022-T3","2022 trimestre 3"),
    ("2023-T1","2023 trimestre 1"), ("2023-T2","2023 trimestre 2"), ("2023-T3","2023 trimestre 3"),
    # reforzamientos
    ("2017-R1","2017 reforzamiento 1"), ("2017-R2","2017 reforzamiento 1"),
    ("2018-R1","2018 reforzamiento 1"), ("2018-R2","2018 reforzamiento 2"),
    ("2018-R1","2019 reforzamiento 1"), ("2017-R2","2019 reforzamiento 2"),
    ("2017-R1","2020 reforzamiento 1"), ("2017-R2","2020 reforzamiento 2"),
    ("2017-R1","2021 reforzamiento 1"), ("2017-R2","2021 reforzamiento 2"),
    ("2017-R1","2022 reforzamiento 1"), ("2017-R2","2022 reforzamiento 2"),
    ("2017-R1","2023 reforzamiento 1"), ("2017-R2","2023 reforzamiento 2"),
    # por bimestres
    ("2017-B1","2017 bimestre 1"), ("2017-B2","2017 bimestre 2"), ("2017-B3","2017 bimestre 3"),
    ("2017-B4","2017 bimestre 4"),
    ("2018-B1","2018 bimestre 1"), ("2018-B2","2018 bimestre 2"), ("2018-B3","2018 bimestre 3"),
    ("2018-B4","2018 bimestre 4"),
    ("2019-B5","2019 bimestre 1"), ("2019-B2","2019 bimestre 2"), ("2019-B2","2019 bimestre 3"),
    ("2019-B2","2019 bimestre 4"),
    ("2020-B1","2020 bimestre 1"), ("2020-B2","2020 bimestre 2"), ("2020-B2","2020 bimestre 3"),
    ("2020-B2","2020 bimestre 4"),
    ("2021-B1","2021 bimestre 1"), ("2021-B2","2021 bimestre 2"), ("2021-B2","2021 bimestre 3"),
    ("2021-B2","2021 bimestre 4"),
    # semestral
    ("2017-S1","2017 semestre 1"), ("2017-T2","2017 semestre 2"),
    ("2018-S1","2018 semestre 1"), ("2018-T2","2018 semestre 2"),
    ("2019-S1","2019 semestre 1"), ("2019-T2","2019 semestre 2"),
    ("2020-S1","2020 semestre 1"), ("2020-T2","2020 semestre 2"),
    ("2021-S1","2021 semestre 1"), ("2021-T2","2021 semestre 2"),
    ("2022-S1","2022 semestre 1"), ("2022-T2","2022 semestre 2"),
    ("2023-S1","2023 semestre 1"), ("2023-T2","2023 semestre 2"),
)

class School(models.Model):
    principal = models.ForeignKey(
        Principal,
        on_delete=models.CASCADE,
        verbose_name= _("director de la escuela"),
    )

    name = models.CharField(
        verbose_name = _("nombre"),
        max_length=100,
        default="-",
    )
    descripcion = models.CharField(
        verbose_name = _("descripcion"),
        default = "-",
        max_length = 1000,
    )
    address = models.CharField(
        verbose_name = _("direccion"),
        default="-",
        max_length=250
    )

    def __str__(self):
        return ("escuela("+ str(self.id) + ") "+ self.name)

class Course(models.Model):
    school = models.ForeignKey(
        School,
        on_delete=models.CASCADE,
        verbose_name=_("escuela")
    )
    
    teacher = models.ForeignKey(
        Teacher,
        on_delete=models.CASCADE,
        verbose_name = _("profesor")
    )
    name = models.CharField(
        verbose_name=_("nombre"),
        max_length=250,
        default="-"
    )
    academic_period = models.CharField(
        verbose_name = _("periodo academico"),
        max_length=100,
        choices=PERIODOS_ACADEMICOS,
        default="SIN_DEFINIR"
    )
    descripcion = models.CharField(
        verbose_name = _("descripcion"),
        max_length=1500,
        default="-"
    )
    
    def __str__(self):
        return ("curso("+str(self.id)+") "+self.name+" ,periodo_academico: "\
                +self.academic_period)

