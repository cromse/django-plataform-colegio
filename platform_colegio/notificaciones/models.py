from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.core.mail import send_mail
from django.core.urlresolvers import reverse

from django.core.exceptions import ObjectDoesNotExist

from usuarios.models import UserM
from usuarios.models import Student, Teacher, Tutor, Principal
from colegios.models import School, Course

ESTADOS_TAREAS=(
    ("PENDIENTE", "pendiente"),
    ("INCOMPLETA", "incompleta"),
    ("HECHA", "hecha"),
)

class Task(models.Model):
    student = models.ForeignKey(
        Student, 
        on_delete=models.CASCADE,
        verbose_name = _("estudiante")
    )
    course = models.ForeignKey(
        Course, 
        on_delete=models.CASCADE,
        verbose_name = _("curso")
    )

    name = models.CharField(
        verbose_name = _("nombre"),
        max_length = 250,
        default = "-",
    )
    state = models.CharField(
        verbose_name=_("estado"),
        max_length=250,
        choices = ESTADOS_TAREAS,
        default = "PENDIENTE",
    )
    
    description=models.CharField(
        verbose_name=_("descripcion"),
        max_length=2500,
        default="-",
    )
    note=models.IntegerField(
        verbose_name=_("nota"),
        blank=True,
        null=True,
    )
    created=models.DateTimeField(
        verbose_name=_('creacion'),
        null=True,
        auto_now_add=True,
    )
    end=models.DateTimeField(
        verbose_name=_("finalizacion"),
        null=True,
        auto_now_add=True
    )
    
    def __str__(self):
        return ("tarea("+ str(self.id)+ ")"+ self.name +", estado:"+self.state)
    
    #TODO: obtener la lista de estudiantes asignados
    #TODO: obtener al docente asignado
    
class Notification(models.Model):
    teacher=models.ForeignKey(
        Teacher,
        on_delete=models.CASCADE,
        verbose_name = _("profesor")
    )
    course=models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
        verbose_name=_("curso")
    )

    student=models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        verbose_name=_("estudiante"),
        blank=True,
        null=True,
    )
    task=models.ForeignKey(
        Task,
        on_delete=models.CASCADE,
        verbose_name=_("tarea asignada"),
        blank=True,
        null=True
    )
    description=models.CharField(
        verbose_name=_("descripcion"),
        max_length=2500,
        default="-"
    )
    created=models.DateTimeField(
        verbose_name=_('creacion'),
        auto_now_add=True,
    )
    revised=models.BooleanField(
        verbose_name=_("revisada"),
        default=False
    )

    def get_task_name(self):
        try:
            t = Task.objects.get(id=self.task)
            return t.name
        except ObjectDoesNotExist:
            return "" # o retornar None?

    def get_student_name(self):
        try:
            s = Student.objects.get(id=self.student)
            return s.get_full_name()
        except ObjectDoesNotExist:
            return "" # o retornar None?

    def __str__(self):
        t = Teacher.objects.get(id=self.teacher)
        c = Course.objects.get(id=self.course)
        return ("notificacion("+ str(self.id) +"), prof:"+t.get_full_name()+\
                ", course:"+c.name)

        

    
