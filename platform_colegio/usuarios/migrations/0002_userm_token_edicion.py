# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-14 00:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userm',
            name='token_edicion',
            field=models.CharField(blank=True, max_length=128, verbose_name='token de edicion'),
        ),
    ]
