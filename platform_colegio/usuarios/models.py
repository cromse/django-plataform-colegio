# -*- encoding:utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.core import validators
from django.core.mail import send_mail
from django.core.urlresolvers import reverse

class MyUserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        # if not email:
            # raise ValueError('Users must have an email address')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            is_superuser=True,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password, *kwargs):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

# tabla user personalizada (heredando de AbstractBaseUser)
class UserM(AbstractUser):
    objects = MyUserManager()

    username = models.CharField(
        _('username'),
        max_length=254,
        unique=True,
        help_text=_('Required. 254 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Enter a valid username. This value may contain only '
                  'letters, numbers ' 'and @/./+/-/_ characters.')
            ),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
        db_index=True,
    )

    first_name = models.CharField(
        verbose_name= _('nombres'), 
        max_length=125, 
        blank=False
    )

    last_name = models.CharField(
        verbose_name= _('apellidos'),
        max_length=125,
        blank=False
    )

    token_edicion = models.CharField(
        verbose_name = _("token de edicion"),
        max_length=128,
        blank=True
    )

    is_admin = models.BooleanField(
        verbose_name = _("es admin"),
        default=False
    )

    # requeridos por AbstractUser
    USERNAME_FIELD = 'username'
    REQUIERED_FIELDS = ['first_name', 'last_name']

    def __str__(self):
        return self.username
    
    def get_full_name(self):
        return (self.first_name+" "+self.last_name)

    def change_password(self, new_password):
        self.set_password(new_password)
        self.save()

    def send_email(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    # def save(self, *args, **kwargs):
        ''' asegurando que no haya otro usuario con los first_name , last_name'''
        # modificar lo siguiente
        # if not self.pk and self.has_usable_password() is False:
        #     self.set_password(self.password)

        # super(User, self).save(*args, **kwargs)

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

class Tutor(models.Model):

    user = models.OneToOneField(
        UserM,                   # notar que hace referencia a tabla personalizada UserM
        on_delete=models.CASCADE,
        verbose_name = _('Tutor'),
    ) 
    
    #TODO: agregar payment

    phone = models.CharField(
        verbose_name = _('telefono'),
        max_length = 100,
        default = "-",
    )
    ci = models.IntegerField(
        verbose_name = _('C.I.'),
        default = 0
    )
    address_home = models.CharField(
        verbose_name = _('direccion casa'),
        max_length = 250,
        default = "-"
    )
    
    receive_notifications = models.BooleanField(
        verbose_name = _('recibe notificaciones?'),
        default = False
    )

    def __str__(self):
        u = UserM.objects.get(id=self.user)
        return ("Tutor("+ str(self.id) + ") "+ u.get_full_name() +\
                ", phone:" + self.phone)

    def natural_key(self):
        return self.username

    def get_email(self):
        u = UserM.objects.get(id=self.user)
        return u.email

    def get_full_name(self):
        u = UserM.objects.get(id=self.user)
        return u.get_full_name()

    def send_email(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.get_email()], **kwargs)

    #def send_sms(self, message):
        #TODO: implementar metodos necesarios para esta funcion
        # ..
    
class Principal(models.Model):
    
    user = models.OneToOneField(
        UserM, 
        on_delete=models.CASCADE,
        verbose_name = _('Director'),
    ) 
    
    #TODO: agregar payment

    phone = models.CharField(
        verbose_name = _('telefono'),
        max_length = 100,
        default = "-",
    )
    ci = models.IntegerField(
        verbose_name = _('C.I.'),
        default = 0
    )
    
    def __str__(self):
        u = UserM.objects.get(id=self.user)
        return ("Principal("+ str(self.id) + ") " \
                + u.get_full_name() +", phone:" + self.phone)

    def get_email(self):
        u = UserM.objects.get(id=self.user)
        return u.email

    def get_full_name(self):
        u = UserM.objects.get(id=self.user)
        return u.get_full_name()

    def send_email(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.get_email()], **kwargs)

    #def send_sms(self, message):
        #TODO: implementar metodos necesarios para esta funcion
        # ..

class Teacher(models.Model):
    # django lo llama `user_id'
    user = models.OneToOneField(
        UserM, 
        on_delete=models.CASCADE,
        verbose_name = _('Profesor'),
    ) 
    
    #TODO: agregar payment

    phone = models.CharField(
        verbose_name = _('telefono'),
        max_length = 100,
        default = "-",
    )
    ci = models.IntegerField(
        verbose_name = _('C.I.'),
        default = 0
    )
    speciality = models.CharField(
        verbose_name=_('especialidad'),
        max_length=200,
        default='-',
    )

    def __str__(self):
        u = UserM.objects.get(id=self.user)
        return ("Profesor(" + str(self.id) + ") "+ u.get_full_name() +\
                ", phone:" + self.phone)

    def get_email(self):
        u = UserM.objects.get(id=self.user)
        return u.email

    def get_full_name(self):
        u = UserM.objects.get(id=self.user)
        return u.get_full_name()

    def send_email(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.get_email()], **kwargs)

    #def send_sms(self, message):
        #TODO: implementar metodos necesarios para esta funcion
        # ..

class Student(models.Model):
    user = models.ForeignKey(
        UserM, 
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    tutor1 = models.ForeignKey(
        Tutor,
        on_delete=models.CASCADE,
        verbose_name= _('Tutor 1'),
        related_name="Tutor_principal", # necesario cuando son varias llaves foraneas
    )
    tutor2 = models.ForeignKey(
        Tutor,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name= _('Tutor 2'),
        related_name="Tutor_secundario1", # necesario
    )
    tutor3 = models.ForeignKey(
        Tutor,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name= _('Tutor 3'),
        related_name="Tutor_secundario2", # necesario
    )
    
    phone = models.CharField(
        verbose_name = _('telefono'),
        max_length = 100,
        default = "-",
    )
    ci = models.IntegerField(
        verbose_name = _('C.I.'),
        default = 0
    )
    address_home = models.CharField(
        verbose_name = _('direccion casa'),
        max_length = 250,
        default = "-"
    )
        
    def __str__(self):
        u = UserM.objects.get(id=self.user)
        return ("Estudiante(" + str(self.id) + ") "+ u.get_full_name() +\
                ", phone:" + self.phone)

    def get_email(self):
        u = UserM.objects.get(id=self.user)
        return u.email

    def get_full_name(self):
        u = UserM.objects.get(id=self.user)
        return u.get_full_name()

    def send_email(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.get_email()], **kwargs)

    #def send_sms(self, message):
        #TODO: implementar metodos necesarios para esta funcion
        # ..
    
    
    
    
    
    
    
