# configuraciones privadas

# Durante produccion poner DEBUG = False
DEBUG = True
DEPLOY = not DEBUG

CLAVE_APP = "clave secreta de la app"

# base de datos
DATABASE_USERNAME = "nombre de usuario de la base de datos"
DATABASE_NAME = "Nombre de la base de datos"
DATABASE_PASSWORD = "Password de la base de datos"
DATABASE_PORT = "Puerto"
DATABASE_HOST = "localhost"

STATIC_URL = "static/"
if DEPLOY:
    STATIC_URL = "digamos /var/www/plataform_colegio/static"
